import React from "react";

const Screen = ({ value }) => <div className="screen">{value}</div>;

Screen.defaultProps = {
  value: "0",
};

export default Screen;
