import React from "react";
import Button from "./Button";

const MathOperations = ({ onClickOperation, onClickEqual }) => (
  <section className="math-operations">
    <Button text="+" clickHandler={onClickOperation} />
    <Button text="-" clickHandler={onClickOperation} />
    <Button text="*" clickHandler={onClickOperation} />
    <Button text="/" clickHandler={onClickOperation} />
  </section>
);

export default MathOperations;
