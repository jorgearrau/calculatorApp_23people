import React from "react";
import Button from "./Button";

const numbers = [7, 8, 9, 4, 5, 6, 1, 2, 3, 0, ".", "AC"];

const renderButtons = (onClickNumber) => {
  const renderButton = (number) => (
    <Button
      key={number}
      text={number.toString()}
      clickHandler={onClickNumber}
    />
  );
  return numbers.map(renderButton);
};

const Numbers = ({ onClickNumber }) => (
  <section className="numbers">{renderButtons(onClickNumber)}</section>
);

export default Numbers;
