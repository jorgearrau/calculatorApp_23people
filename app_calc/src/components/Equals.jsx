import React from "react";
import Button from "./Button";

const Equals = ({ onClickOperation, onClickEqual }) => (
  <section className="equal-operations">
    <div>
      <Button text="=" clickHandler={onClickEqual} />
    </div>
  </section>
);

export default Equals;
