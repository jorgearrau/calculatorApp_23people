import React, { useState } from "react";
import words from 'lodash.words';
import MathOperations from "./components/MathOperations";
import Equals from "./components/Equals";
import Numbers from "./components/Numbers";

import Screen from "./components/Screen";
import "./App.css";

const App = () => {
  const [stack, setStack] = useState("");
  //Se crea esta funcion utilizando libreria lodash para ocultar los signos de operacion con la construccion de un array
  const items = words(stack, /[^-^+^*^/]+/g)

  return (
    <main className="react-calculator">
      <Screen value={items[items.length-1]} />

      <MathOperations
        onClickOperation={(operation) => {
          setStack(`${stack}${operation}`);
        }}
        onClickEqual={(equal) => {
          // eslint-disable-next-line
          setStack(`${stack}${eval}`);
          //setStack(eval(stack));
        }}
      />

      <Numbers
        onClickNumber={(number) => {
          if (number === "AC") {
            setStack(` `);
          } else {
            setStack(`${stack}${number}`);
          }
        }}
      />

      <Equals
        onClickEqual={(equal) => {
          // eslint-disable-next-line
          setStack(eval(stack));
        }}
      />
    </main>
  );
};

export default App;
